<div align="center">-

# Organización del computador II – UNGS 2do cuatrimestre del 2021 
<img src="/img/duck.gif" width="180" height="180"/>

## TP II - Enmascarar con SIMD
**Daniel y Gabriel Carrillo**
</div>

---

# Tabla de Contenidos
1. [Clonar y ejecutar el programa.](#titulo1)
2. [Como usar el menú de ejecución.](#titulo2)
3. [Problemas, decisiones y evolución en el código.](#titulo3)
    1. [Enmascarar en C.](#subtitulo3.1)
    2. [Enmascarar en Assembler.](#subtitulo3.2)
4. [Capturas de funcionamiento de los programas.](#titulo4)
    1. [Imagen producida en Lenguaje C.](#subtitulo4.1)
    2. [Imagen producida en Assembler.](#subtitulo4.2)
5. [Gráfica de rendimiento Enmascarar Lenguaje C. vs Assember SIMD.](#titulo5)
    1. [Gráfico medido en tiempo (segundos).](#subtitulo5.1)
    2. [Tabla de comparción con datos en tiempo (segundos).](#subtitulo5.2)
    3. [Gráfico medido en ticks.](#subtitulo5.3)
    4. [Tabla de comparción con datos en ticks.](#subtitulo5.4)
5. [Observaciones generales.](#titulo6)
6. [Conclusión.](#titulo7)
---

## Clonar y ejecutar el programa:<a name="titulo1"></a>

Para clonar el programa ejectucar el siguiente comando:

### `git clone https://gitlab.com/mangostas_luchadoras/tp2_orga_2.git`

Para iniciar el programa ejecutar el escript de bash de la siguente manera:

### `./init_menu.sh`

---
## Como usar el menú de ejecución:<a name="titulo2"></a>
<div align="center">
<img src="/img/menu.png" width="700" height="500"/>
</div>
_a.  a.  Compilar programa enmascarar con Lenguaje C_ <br />
_b.  b.  Compilar programa enmascarar con Assembler_ <br /><br />
Elegir cualquiera de estas opciones para generar el archivo objeto y el ejecutable del programa seleccionado. Es necesario primero compilar para luego ejecutar.<br /><br />
_c.  Ejecutar programa enmascarar con Lenguaje C_ <br />
_d.  Ejecutar programa enmascarar con Assembler_ <br /><br />
Elegir cualquiera de estas opciones para ejecutar el programa seleccionado.

---
## Problemas, decisiones y evolución en el código:<a name="titulo3"></a>

### Enmascarar en C <a name="subtitulo3.1"></a>

Luego de varios problemas a la hora de manejar la lectura y escritura de imagenes, se opto por realizar una serie de structs para formar la estructura del archivo .bmp, se utilizó un ejemplo encontrado en internet al cual se le realizaron modificaciones para que se aplicara al caso.

Estructura de las imagenes:

```
struct BITMAP_header {
  char name[2];
  unsigned int size;
  int garbage;
  unsigned int image_offset;
 };

struct DIB_header {
  unsigned int header_size;
  unsigned int width;
  unsigned int heigth;
  unsigned short int colorsplanes;
  unsigned short int bitsperpixel;
  unsigned int compression;
  unsigned int image_size;
  unsigned int temp[25];
};

struct RGB {
  unsigned char blue;
  unsigned char green;
  unsigned char red;
};

struct Image {
  int height;
  int width;
  struct RGB **rgb;
};

struct BMP {
  struct BITMAP_header header;
  struct DIB_header dibheader;
  struct Image image;
};


```

Código para enmascarar:

```
void enmascarar(struct Image picA, struct Image picB, struct Image picM){
    for (int i=0; i<picM.height;i++){
      for(int j=0; j<picM.width; j++){
        if(picM.rgb[i][j].red == 0x00 && picM.rgb[i][j].green == 0x00 && picM.rgb[i][j].blue== 0x00){
          picA.rgb[i][j].red = picB.rgb[i][j].red;
          picA.rgb[i][j].green = picB.rgb[i][j].green;
          picA.rgb[i][j].blue = picB.rgb[i][j].blue;
        } 
      }  
    }  
}
```

### Enmascarar en Assembler: <a name="subtitulo3.2"></a>

La lógica para enmascarar en Assembler fue sencilla, se opto por utilizar el siguiente auxiliar:

```
aux db 0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF

```
Para luego generar un loop al recorrer las imagenes, donde se realiza un operador logico and de una imagen con la mascara y se genera una nueva mascara invertida restando la mascara con el auxiliar para aplicarle a la otra imagen.

```
_loop:
    cmp edx, 0
    jz _leave
    
    movq mm0, qword[eax]
    movq mm1, qword[ebx]
    movq mm5, qword[ecx]
    movq mm6, mm7
    
    psubusb mm6, mm5
    
    pand mm0, mm5
    pand mm1, mm6
    
    por mm0, mm1

    movq qword[eax], mm0

    add eax, 8
    add ebx, 8
    add ecx, 8
    sub edx, 8
    
    jmp _loop
```

Se tuvo que modificar la lectura y escritura de datos en C por problemas al momento de pasar como parametros los struct a la función en Assembler, por ejemplo la lectura de imagen en C:

```
unsigned char* readImage(const char *file, int *tamanio) {
  FILE *fp = fopen(file, "rb");
  int offset;

  fseek(fp, PIXELS_OFFSET_OFFSET, SEEK_SET);
  fread(&offset, sizeof(int), 1, fp);

  fseek(fp, SIZE_OFFSET, SEEK_SET);
  fread(tamanio, sizeof(int), 1, fp);
  
  unsigned char *buffer = (unsigned char *) malloc(sizeof(unsigned char) * (*tamanio));

  fseek(fp, offset, SEEK_SET);
  fread(buffer, sizeof(unsigned char), *tamanio, fp);

  fclose(fp);

  return buffer;
}
```

## Capturas de funcionamiento de los programas:<a name="titulo4"></a>

### Imagen producida en Lenguaje C  <a name="subtitulo4.1"></a>
<div align="center">
<img src="/img/salida_c.png" width="600" height="600"/>
</div>

### Imagen producida en Assembler <a name="subtitulo4.2"></a>

<div align="center">
<img src="/img/salida_asm.png" width="600" height="600"/>
</div>

En este caso se utilizan imagenes rojas y azules con mascaras blanco y negro con dibujos. Como se puede apreciar, el resultado a nivel visual en ambos casos es el mismo.

## Gráfica de rendimiento Enmascarar Lenguaje C. vs Assember SIMD.<a name="titulo5"></a>

### Gráfico medido en tiempo (segundos) <a name="subtitulo5.1"></a>

<div align="center">
<img src="/img/tiempo.jpeg" width="800" height="500"/> 
</div>

### Tabla de comparción con datos en tiempo (segundos) <a name="subtitulo5.2"></a>

<div align="center">
<img src="/img/ttiempo.jpeg" width="400" height="400"/>
</div>

### Gráfico medido en ticks <a name="subtitulo5.3"></a>

<div align="center">
<img src="/img/ticks.jpeg" width="800" height="500"/>
</div>

### Tabla de comparción con datos en ticks <a name="subtitulo5.4"></a>

<div align="center">
<img src="/img/tticks.jpeg" width="400" height="400"/>
</div>

## Observaciones generales:<a name="titulo6"></a>

* Los programas desarrollados estan pensados para ser ejecutados en un sistema operativo con una distribución de Linux, por lo que se recomienda su utilización.
* Para realizar el script de ejecución del trabajo en bash. Se opto por reutilizar la base de un menú realizado anteriormente para un trabajo de SOR I `code reuse`
* Para la realización del documento README.md se reutilizó la estructura del TP1 de OrgaII.
* A lo largo del trabajo se presentaron problemas, por la forma de encarar el tratamiento de las imagenes en C, ante algunas dificultades se termino realizando código más complejo del necesesario, pero el caso sirvió para mejorar el aprendisaje.
* Respecto a la parte de Assembler no presento gran difícultad, pero si se sumó a última hora el problema que se arrastraba de C en la lectura y escritura de archivos.
* Finalmente el trabajo resulta interesante para llevar a concreto lo aprendido durante la materia sobre SIMD y MMX

## Conclusión:<a name="titulo7"></a>
<div align="center">
<img src="/img/homer.gif"/>
</div>
