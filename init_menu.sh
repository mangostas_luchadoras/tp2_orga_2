#!/bin/bash
#------------------------------------------------------
# PALETA DE COLORES
#------------------------------------------------------
#setaf para color de letras/setab: color de fondo
	black=$(tput setaf 0);
	bg_red=$(tput setab 1);
	reset=$(tput sgr0);
	bold=$(tput setaf bold);

#------------------------------------------------------
# DIRECTORIO DE TRABAJO
#------------------------------------------------------

proyectoActual=$(pwd);

#------------------------------------------------------
# DISPLAY MENU
#------------------------------------------------------

imprimir_menu () {
    imprimir_encabezado "  ██    ███ ███████ ███    ██ ██    ██     ████████ ██████  ██████   \n\t\t ████  ████ ██      ████   ██ ██    ██        ██    ██   ██      ██  \n\t\t ██ ████ ██ █████   ██ ██  ██ ██    ██        ██    ██████   █████   \n\t\t ██  ██  ██ ██      ██  ██ ██ ██    ██        ██    ██      ██       \n\t\t ██      ██ ███████ ██   ████  ██████         ██    ██      ███████  \n\t\t                                                                     \n\t\t                                                                     \n\t\t          ██████  ██████   ██████   █████      ██ ██                 \n\t\t         ██    ██ ██   ██ ██       ██   ██     ██ ██                 \n\t\t         ██    ██ ██████  ██   ███ ███████     ██ ██                 \n\t\t         ██    ██ ██   ██ ██    ██ ██   ██     ██ ██                 \n\t\t          ██████  ██   ██  ██████  ██   ██     ██ ██                 \n\t\t                                                                     ";  
    echo -e "\t\t El directorio de trabajo es:";
    echo -e "\t\t $proyectoActual";

    echo -e "\t\t";
    echo -e "\t\t Opciones:";
    echo "";
    echo -e "\t\t\t a.  Compilar programa enmascarar con Lenguaje C";
    echo -e "\t\t\t b.  Compilar programa enmascarar con Assembler";
    echo -e "\t\t\t c.  Ejecutar programa enmascarar con Lenguaje C";
    echo -e "\t\t\t d.  Ejecutar programa enmascarar con Assembler";
    
    echo -e "\t\t\t q.  Salir";
    echo "";
    echo -e "Escriba la opción y presione ENTER";
}

#------------------------------------------------------
# FUNCTIONES AUXILIARES
#------------------------------------------------------

imprimir_encabezado () {
    clear;
    #Se le agrega formato a la fecha que muestra
    #Se agrega variable $USER para ver que usuario está ejecutando
    echo -e "`date +"%d-%m-%Y %T" `\t\t\t\t USERNAME:$USER";
    echo "";
    #Se agregan colores a encabezado
    echo -e "\t\t${bg_red}${black}${bold}---------------------------------------------------------------------\t${reset}";
    echo -e "\t\t${bold}${bg_red}${black}$1\t\t${reset}";
    echo -e "\t\t${bg_red}${black}${bold}---------------------------------------------------------------------\t${reset}";
    echo "";
}

esperar () {
    echo "";
    echo -e "Presione enter para continuar";
    read ENTER ;
}

malaEleccion () {
    echo -e "Selección Inválida ..." ;
}

decidir () {
	echo $1;
	while true; do
		echo "desea ejecutar? (s/n)";
    		read respuesta;
    		case $respuesta in
        		[Nn]* ) break;;
       			[Ss]* ) eval $1
				break;;
        		* ) echo "Por favor tipear S/s ó N/n.";;
    		esac
	done
}

#------------------------------------------------------
# FUNCTIONES del MENU
#------------------------------------------------------

a_funcion () {
  	imprimir_encabezado " Opción a.                                                           \n\t\t Compilar programa enmascarar con Lenguaje C                         ";
    decidir "gcc -m32 enmascarar_c.c -o enmascarar_c";
}

b_funcion () {
   	imprimir_encabezado " Opción b.                                                           \n\t\t Compilar programa enmascarar con Assembler                          ";
    decidir "nasm -f elf enmascarar_asm.asm -o enmascarar_asm.o; gcc -m32 enmascarar_asm.c enmascarar_asm.o -o enmascarar_asm -no-pie";

}

c_funcion () {
   	imprimir_encabezado " Opción c.                                                           \n\t\t Ejecutar programa enmascarar con Lenguaje C                         ";
   	echo -e "Ingrese el nombre de la primer imagen"
    read img0;
    echo -e "Ingrese el nombre de la segunda imagen"
    read img1;
    echo -e "Ingrese el nombre de la mascara de imagen"
    read mascara;
    echo -e "Ingrese el nombre de la imagen de salida"
    read salida;
    convert $img0 -define colorspace:auto-grayscale=false -type truecolor $img0
    convert $img1 -define colorspace:auto-grayscale=false -type truecolor $img1
    convert $mascara -define colorspace:auto-grayscale=false -type truecolor $mascara
    decidir " ./enmascarar_c $img0 $img1 $mascara $salida";
}

d_funcion () {
    imprimir_encabezado " Opción d.                                                           \n\t\t Ejecutar programa enmascarar con Assembler                          ";
    echo -e "Ingrese el nombre de la primer imagen"
    read img0;
    echo -e "Ingrese el nombre de la segunda imagen"
    read img1;
    echo -e "Ingrese el nombre de la mascara de imagen"
    read mascara;
    echo -e "Ingrese el nombre de la imagen de salida"
    read salida;
    convert $img0 -define colorspace:auto-grayscale=false -type truecolor $img0
    convert $img1 -define colorspace:auto-grayscale=false -type truecolor $img1
    convert $mascara -define colorspace:auto-grayscale=false -type truecolor $mascara
    decidir " ./enmascarar_asm $img0 $img1 $mascara $salida";
}

q_funcion () {
	clear;
	exit;
}

#------------------------------------------------------
# LOGICA PRINCIPAL
#------------------------------------------------------

while  true
do
    # 1. mostrar el menu
    imprimir_menu;
    # 2. leer la opcion del usuario
    read opcion;

    case $opcion in
        a|A) a_funcion;;
        b|B) b_funcion;;
        c|C) c_funcion;;
        d|D) d_funcion;;
        q|Q) q_funcion;;

        *) malaEleccion;;
    esac
    esperar;
done
