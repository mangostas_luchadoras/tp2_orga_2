#include <stdio.h>
#include <stdlib.h>

#define PIXELS_OFFSET_OFFSET 0x0A
#define SIZE_OFFSET 0x22

extern void enmascarar_asm(unsigned char *imgA, unsigned char *imgB, unsigned char *imgM, int tam);

unsigned char* readImage(const char *file, int *tamanio) {
  FILE *fp = fopen(file, "rb");
  int offset;

  fseek(fp, PIXELS_OFFSET_OFFSET, SEEK_SET);
  fread(&offset, sizeof(int), 1, fp);

  fseek(fp, SIZE_OFFSET, SEEK_SET);
  fread(tamanio, sizeof(int), 1, fp);
  
  unsigned char *buffer = (unsigned char *) malloc(sizeof(unsigned char) * (*tamanio));

  fseek(fp, offset, SEEK_SET);
  fread(buffer, sizeof(unsigned char), *tamanio, fp);

  fclose(fp);

  return buffer;
}

unsigned char* readHeader(const char *file, int *offset) {
  FILE *fp = fopen(file, "rb");

  fseek(fp, PIXELS_OFFSET_OFFSET, SEEK_SET);
  fread(offset, sizeof(int), 1, fp);
  rewind(fp);

  unsigned char *buffer = (unsigned char *) malloc(sizeof(unsigned char) * (*offset - 1));

  fread(buffer, sizeof(unsigned char), *offset -1, fp);

  fclose(fp);

  return buffer;
}

void writeImage(const char *file, unsigned char *pix, unsigned char *header, int offset, int tamanio) {
  FILE *fwp = fopen(file, "w");

  fwrite(header, sizeof(unsigned char), offset, fwp);
  fwrite(pix, sizeof(unsigned char), tamanio,fwp);

  fclose(fwp);
}

int main(int argc, char *argv[]) {
  if (argc != 5) {
    
    printf("\nEl uso del comando es:\n%s [imagen1] [imagen2] [mascara] [nombre de imagen enmascarada]\n\n", argv[0]);
    return 0;

  } else {
    unsigned char *buffer1, *buffer2, *buffer3, *header;
    int tamanio, offset;

    buffer1 = readImage(argv[1], &tamanio);
    buffer2 = readImage(argv[2], &tamanio);
    buffer3 = readImage(argv[3], &tamanio);
    header = readHeader(argv[1], &offset);

    enmascarar_asm(buffer1, buffer2, buffer3, tamanio);

    writeImage(argv[4], buffer1, header, offset, tamanio);
/*
    for(int i = 0; i < tamanio; i+=3) {
      printf("%x %x %x\n", *(buffer1 + i), *(buffer1 + i + 1), *(buffer1 + i + 2));
    }
*/
    free(buffer1);
    free(buffer2);
    free(buffer3);
    free(header);

    return 0;
  }  
} 