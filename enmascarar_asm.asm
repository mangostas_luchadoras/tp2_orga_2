section .data

aux db 0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF

section .text

global enmascarar_asm

enmascarar_asm:
    push ebp
    mov ebp, esp; for correct debugging
    
    push eax
    push ebx
    push ecx
    push edx

    mov eax, [ebp + 8]
    mov ebx, [ebp + 12]
    mov ecx, [ebp + 16]
    mov edx, [ebp + 20]
        
    movq mm7, qword[aux]

_loop:
    cmp edx, 0
    jz _leave
    
    movq mm0, qword[eax]
    movq mm1, qword[ebx]
    movq mm5, qword[ecx]
    movq mm6, mm7
    
    psubusb mm6, mm5
    
    pand mm0, mm5
    pand mm1, mm6
    
    por mm0, mm1

    movq qword[eax], mm0

    add eax, 8
    add ebx, 8
    add ecx, 8
    sub edx, 8
    
    jmp _loop
    
_leave:    
    pop edx
    pop ecx
    pop ebx
    pop eax

    mov ebp, esp
    pop ebp
    ret