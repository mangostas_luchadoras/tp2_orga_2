#include <stdio.h>
#include <stdlib.h>

struct BITMAP_header {
  char name[2];
  unsigned int size;
  int garbage;
  unsigned int image_offset;
 };

struct DIB_header {
  unsigned int header_size;
  unsigned int width;
  unsigned int heigth;
  unsigned short int colorsplanes;
  unsigned short int bitsperpixel;
  unsigned int compression;
  unsigned int image_size;
  unsigned int temp[25];
};

struct RGB {
  unsigned char blue;
  unsigned char green;
  unsigned char red;
};

struct Image {
  int height;
  int width;
  struct RGB **rgb;
};

struct BMP {
  struct BITMAP_header header;
  struct DIB_header dibheader;
  struct Image image;
};

struct Image readImage(FILE *fp, int height, int width){
  struct Image pic;
  pic.rgb = (struct RGB**) malloc(height*sizeof(void*));
  pic.height = height;
  pic.width = width;
  int bytestoread = ((24 * width +31)/32)*4;
  int numOfrgb = bytestoread/sizeof(struct RGB) + 1;

  for (int i = height-1; i >= 0; i--){
    pic.rgb[i] = (struct  RGB*) malloc(numOfrgb*sizeof(struct RGB));
    fread(pic.rgb[i], 1, bytestoread, fp);
  }
  return pic;
}

void freeImage(struct Image pic){
  for (int i = pic.height -1; i >= 0; i--) free(pic.rgb[i]);
    free(pic.rgb);
}

void enmascarar(struct Image picA, struct Image picB, struct Image picM){
    for (int i=0; i<picM.height;i++){
      for(int j=0; j<picM.width; j++){
        if(picM.rgb[i][j].red == 0x00 && picM.rgb[i][j].green == 0x00 && picM.rgb[i][j].blue== 0x00){
          picA.rgb[i][j].red = picB.rgb[i][j].red;
          picA.rgb[i][j].green = picB.rgb[i][j].green;
          picA.rgb[i][j].blue = picB.rgb[i][j].blue;
        } 
      }  
    }  
}

int createImage(struct BITMAP_header header, struct DIB_header dibheader, 
  struct Image pic, char *nombre){
  
  FILE *fwp = fopen(nombre, "w");
  if (fwp == NULL) return 1;

  fwrite(header.name, 2, 1, fwp);
  fwrite(&header.size, 3*sizeof(int), 1, fwp);
  fwrite(&dibheader, sizeof(struct DIB_header), 1, fwp);

  for (int i = pic.height -1; i >= 0; i--)
    fwrite(pic.rgb[i], ((24 * pic.width +31)/32)*4, 1, fwp);
  
  fclose(fwp);
  return 0;
}
  
struct BMP openbmpfile(const char *fileName) {
  FILE *fp = fopen(fileName,"rb");

  struct BITMAP_header header;
  struct DIB_header dibheader;

  fread(header.name, 2, 1, fp);
  fread(&header.size, 3*sizeof(int), 1, fp);
  fread(&dibheader, sizeof(struct DIB_header), 1, fp);
  fseek(fp, header.image_offset, SEEK_SET);
  struct Image image = readImage(fp,dibheader.heigth, dibheader.width);
  struct BMP bmp;
  bmp.header = header;
  bmp.dibheader = dibheader;
  bmp.image = image;

  fclose(fp);
  return bmp;
}

int main(int argc, char *argv[]) {
  if (argc != 5) {
    
    printf("\nEl uso del comando es:\n%s [imagen1] [imagen2] [mascara] [nombre de imagen enmascarada]\n\n", argv[0]);
    return 0;

  } else {
  
    struct BMP imageA = openbmpfile(argv[1]);
    struct BMP imageB = openbmpfile(argv[2]);
    struct BMP imageM = openbmpfile(argv[3]);
    
    enmascarar(imageA.image, imageB.image, imageM.image);
    
    createImage(imageA.header, imageA.dibheader, imageA.image, argv[4]);

    freeImage(imageA.image);
    freeImage(imageB.image);
    freeImage(imageM.image);

  return 0;
  }
  
} 